cmake_minimum_required( VERSION 3.0 )
project( Caesar )
include_directories( include )

# library
add_library( Caesar-lib
    src/Caesar/Caesar.cpp )

# executable
add_executable( Caesar-app
  app/main.cpp )
target_link_libraries( Caesar-app Caesar-lib )

# install
install( TARGETS Caesar-app DESTINATION bin )

# testing
add_executable( Caesar-test
    test/Caesar/Caesar-test.cpp
    test/main.cpp )
target_link_libraries( Caesar-test Caesar-lib )
enable_testing()
add_test( NAME Caesar-test COMMAND Caesar-test )

